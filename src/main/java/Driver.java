import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;


public class Driver extends Configured implements Tool {
    @Override
    public int run(String[] strings) throws Exception {
        Job job = setup();
        createTable("Employee", "EmployeeDetails");
        loadEmployeeDataToHbase();
        FileOutputFormat.setOutputPath(job, new Path("/ou0"));
        byte[] EMPLOYEE_TABLE = Bytes.toBytes("Employee");
        List<Scan> scanList = new ArrayList<>();
        Scan employeeScan = new Scan();
        employeeScan.setAttribute(Scan.SCAN_ATTRIBUTES_TABLE_NAME, EMPLOYEE_TABLE);
        scanList.add(employeeScan);
        //System.out.println("shubham"+employeeScan.getId());
        TableMapReduceUtil.initTableMapperJob(scanList, HbaseMapper.class, Text.class, ImmutableBytesWritable.class, job);
        return (job.waitForCompletion(true) ? 0 : 1);

    }

    private Job setup() throws IOException {
        Configuration con = new Configuration();
        ;
        Job job = Job.getInstance(con, "Driver");
        job.setJarByClass(Driver.class);
        return job;
    }

    public static void main(String[] args) throws Exception {
        ToolRunner.run(new Driver(), args);

    }

    public void createTable(String tableName, String familyName)
            throws Exception {

        Configuration con = HBaseConfiguration.create();
        try (Connection connection = ConnectionFactory.createConnection(con);
             Admin admin = connection.getAdmin()) {

            HTableDescriptor tableDescriptor = new
                    HTableDescriptor(TableName.valueOf(tableName));

            tableDescriptor.addFamily(new HColumnDescriptor(familyName));

            admin.createTable(tableDescriptor);
        }
    }

    public void loadEmployeeDataToHbase() throws Exception {
        try {
            FileSystem fs = FileSystem.get(new URI("hdfs://localhost:9000"), new Configuration());
            FileStatus[] status = fs.listStatus(new Path("hdfs://localhost:9000/user/data/dir1/Employee.csv"));

            for (int i = 0; i < status.length; i++) {
                BufferedReader br = new BufferedReader
                        (new InputStreamReader(fs.open(status[i].getPath())));
                String line;
                line = br.readLine();
                while (line != null) {
                    String[] values = line.split(",");

                    // Employee.employee.Builder employee = Employee.employee.newBuilder();
                    Employee.employee.Builder employee = Employee.employee.newBuilder();

                    employee.setName(values[0]);
                    employee.setEmployeeId((values[1]));
                    employee.setBuildingCode(values[2]);
                    employee.setFloor(Employee.employee.floors.
                            forNumber(Integer.parseInt(values[3])));
                    employee.setSalary(Integer.parseInt(values[4]));
                    employee.setDepartment(values[5]);

                    Configuration config = HBaseConfiguration.create();

                    Connection hbconnection =
                            ConnectionFactory.createConnection(config);
                    Table table = hbconnection.
                            getTable(TableName.valueOf("Employee"));

                    Put p = new Put(Bytes.toBytes(employee.getEmployeeId()));
                    p.addColumn(Bytes.toBytes("EmployeeDetails"),
                            Bytes.toBytes("EmployeeProto"),
                            employee.build().toByteArray());
                    table.put(p);
                    line = br.readLine();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
