import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;

import java.io.IOException;

public class HbaseMapper extends TableMapper<Text, ImmutableBytesWritable> {
    @Override
    protected void map(ImmutableBytesWritable key, Result value, Context context) throws IOException, InterruptedException {
        byte[] COLUMN_FAMILY = Bytes.toBytes("EmployeeDetails");
        byte[] COLUMN_IDENTIFIER = Bytes.toBytes("EmployeeProto");
        String EMPLOYEE_TABLE = "Employee";

        Employee.employee e = Employee.employee.parseFrom(value.getValue(COLUMN_FAMILY, COLUMN_IDENTIFIER));
        Employee.employee.Builder newEmployeeBuilder = Employee.employee.newBuilder();
        newEmployeeBuilder.setName(e.getName());
        newEmployeeBuilder.setEmployeeId(e.getEmployeeId());
        newEmployeeBuilder.setBuildingCode(e.getBuildingCode());
        newEmployeeBuilder.setFloor(e.getFloor());
        newEmployeeBuilder.setSalary(e.getSalary());
        newEmployeeBuilder.setDepartment(e.getDepartment());
        newEmployeeBuilder.setCafeteriaCode(1);

        Configuration conf = HBaseConfiguration.create();
        Connection connection = ConnectionFactory.createConnection(conf);
        Table hBaseTableObject = connection.getTable(TableName.valueOf(EMPLOYEE_TABLE));
        Put put = new Put(key.get());
        put.addColumn(COLUMN_FAMILY, COLUMN_IDENTIFIER, newEmployeeBuilder.build().toByteArray());

    }
}
